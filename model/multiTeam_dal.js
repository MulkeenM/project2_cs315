var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getMultiGames = function(callback) {

    var query = 'SELECT * FROM Multi_Teams m left join Games g on g.game_id = m.game_id group by game_title;';

    connection.query(query, function(err, result) {
        callback(err, result)
    });
};

exports.getAll = function(callback) {
    var query = 'SELECT m.*, g.game_title FROM Multi_Teams m ' +
    'left join Games g on g.game_id = m.game_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO Multi_Teams (team_name, win_ratio, game_id) VALUES (?,?,?)';

    var queryData = [params.team_name, params.win_ratio, params.game_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

};

exports.delete = function(team_id, callback ) {

    var query = 'DELETE FROM Multi_Teams WHERE team_id = ?';

    var queryData = [team_id];


    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE game SET game_title = ?, single_player = ?, multi_player = ? WHERE game_id = ?';
    var queryData = [params.game_title, params.single_player, params.multi_player, params.game_id];

    connection.query(query, queryData, function (err, result) {
        // DELETE the old stuff

        callback(err, result);
    });


};

exports.getById = function(team_id, callback) {
    var query = 'SELECT * FROM Multi_Teams WHERE team_id = ?';
    var queryData = [team_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};