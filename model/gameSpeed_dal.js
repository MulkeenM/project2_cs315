var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);
exports.view = function(callback) {
    var query = 'SELECT * FROM max_speed;';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getInfo = function(callback) {
    var query = 'Select g.game_title, sp.user_name, g.game_id, sp.Splayer_id FROM Games g left join Game_Speed gs ON gs.game_id = g.game_id left join Speed_Runners sp on sp.Splayer_id = gs.Splayer_id where g.single_player = 1 ;';
    /*
    var query = 'Call gameSpeed_getinfo(?)';
    var queryData = [Splayer_id];
    */
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};
exports.getAll = function(callback) {

    var query = 'SELECT DISTINCT(g.game_title), rs.user_name, best_time FROM Game_Speed gs ' +
        'left join Games g on g.game_id = gs.game_id ' +
        'left join Speed_Runners rs on rs.Splayer_id = gs.Splayer_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO Game_Speed (game_id, Splayer_id, best_time) VALUES (?,?,?)';

    var queryData = [params.game_id, params.Splayer_id, params.best_time];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete = function(game_id, callback ) {

    var query = 'DELETE FROM Game_Speed WHERE game_id = ? ';

    var queryData = [game_id];


    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE game SET game_title = ?, single_player = ?, multi_player = ? WHERE game_id = ?';
    var queryData = [params.game_title, params.single_player, params.multi_player, params.game_id];

    connection.query(query, queryData, function (err, result) {
        // DELETE the old stuff

        callback(err, result);
    });


};

exports.getById = function(game_id, callback) {
    var query = 'SELECT * FROM Games WHERE game_id = ?';
    var queryData = [game_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};