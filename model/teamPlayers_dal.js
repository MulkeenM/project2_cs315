var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getTeamNames = function(callback) {

    var query = 'SELECT * FROM Team_Players p left join Multi_Teams m on m.team_id = m.team_id group by m.team_name';

    connection.query(query, function(err, result) {
        callback(err, result)
    });
};

exports.getAll = function(callback) {
    var query = 'SELECT * FROM Team_Players p left join Multi_Teams m on m.team_id = m.team_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO Team_Players (user_name, team_id) VALUES (?,?)';

    var queryData = [params.user_name, params.team_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

};

exports.delete = function(player_id, callback ) {

    var query = 'DELETE FROM Team_Players WHERE player_id = ?';

    var queryData = [player_id];


    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.getById = function(team_id, callback) {
    var query = 'SELECT * FROM Multi_Teams WHERE team_id = ?';
    var queryData = [team_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};