var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getGameAvgScores = function(score, callback) {
    var query = 'SELECT game_title, avg(float_score) as avg_score FROM Games g JOIN Reviews r ON  r.game_id = g.game_id GROUP BY g.game_id HAVING avg(float_score) > (?) ORDER BY avg_score desc;';
    var queryData = [score];
    connection.query(query, queryData, function(err, result){
        callback(err,result)
    });
};


exports.getReviewGames = function(callback) {

    var query = 'SELECT * FROM Reviews r left join Games g on g.game_id = r.game_id group by game_title;';

    connection.query(query, function(err, result) {
        callback(err, result)
    });
};

exports.getAll = function(callback) {
    var query = 'SELECT *, g.game_title FROM Reviews ' +
        'left join Games g on g.game_id = Reviews.game_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO Reviews (reviewer, float_score, game_id) VALUES (?,?,?)';

    var queryData = [params.reviewer, params.float_score, params.game_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

};

exports.delete = function(review_id, callback ) {

    var query = 'DELETE FROM Reviews WHERE review_id = ?';

    var queryData = [review_id];


    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE game SET game_title = ?, single_player = ?, multi_player = ? WHERE game_id = ?';
    var queryData = [params.game_title, params.single_player, params.multi_player, params.game_id];

    connection.query(query, queryData, function (err, result) {
        // DELETE the old stuff

        callback(err, result);
    });


};

exports.getById = function(review_id, callback) {
    var query = 'SELECT Reviews.*, g.game_title FROM Reviews ' +
        'left join Games g on g.game_id = Reviews.game_id ' +
        'WHERE review_id = ?';
    var queryData = [review_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};