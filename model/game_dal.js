var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM Games;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO Games (game_title, date, single_player, multi_player) VALUES (?,?,?,?)';

    var queryData = [params.game_title, params.single_player, params.multi_player];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(game_id, callback ) {

    var query = 'DELETE FROM Games WHERE game_id = ?';

    var queryData = [game_id];


    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE game SET game_title = ?, single_player = ?, multi_player = ? WHERE game_id = ?';
    var queryData = [params.game_title, params.single_player, params.multi_player, params.game_id];

    connection.query(query, queryData, function (err, result) {
        // DELETE the old stuff

        callback(err, result);
    });


};

exports.getById = function(game_id, callback) {
    var query = 'SELECT * FROM Games WHERE game_id = ?';
    var queryData = [game_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};