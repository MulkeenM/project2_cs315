var express = require('express');
var router = express.Router();
var teamPlayers_dal = require('../model/teamPlayers_dal');

// View All companys
router.get('/all', function(req, res) {
    teamPlayers_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('teamPlayers/teamPlayersViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        teamPlayers_dal.getById(req.query.player_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('teamPlayer/teamPlayerViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    teamPlayers_dal.getTeamNames(function(err, result) {

        if (err) {
            res.send(err);
        }
        else {
            res.render('teamPlayers/teamPlayersAdd', {'team': result});
        }

    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.user_name == null) {
        res.send('User name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        teamPlayers_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/teamPlayers/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('A account id is required');
    }
    else {
        game_dal.edit(req.query.account_id, function(err, result){
            res.render('game/gameUpdate', {game: result[0][0]});
        });
    }

});


router.get('/update', function(req, res) {
    game_dal.update(req.query, function(err, result){
        res.redirect(302, '/game/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        teamPlayers_dal.delete(req.query.player_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/teamPlayers/all');
            }
        });
    }
});

module.exports = router;