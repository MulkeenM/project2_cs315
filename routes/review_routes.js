var express = require('express');
var router = express.Router();
var review_dal = require('../model/review_dal');



// View All companys
router.get('/all', function(req, res) {
    review_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('review/reviewViewAll', { 'result':result });
        }
    });

});

router.get('/AvgScores', function(req, res){
    review_dal.getGameAvgScores(function(err, result){
        if (err) {
            res.send(err);
        }
        else {
            res.render('review/reviewAvgScore', { 'result':result});
        }
    });
});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.review_id == null) {
        res.send('review_id is null');
    }
    else {
        review_dal.getById(req.query.review_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('review/reviewViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    review_dal.getReviewGames(function(err, result) {

        if (err) {
            res.send(err);
        }
        else {
            res.render('review/reviewAdd', {'game': result});
        }

    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.reviewer == null) {
        res.send('Reviewer must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        review_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/review/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.review_id == null) {
        res.send('A account id is required');
    }
    else {
        game_dal.edit(req.query.account_id, function(err, result){
            res.render('review/reviewUpdate', {review: result[0][0]});
        });
    }

});


router.get('/update', function(req, res) {
    game_dal.update(req.query, function(err, result){
        res.redirect(302, '/game/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.review_id == null) {
        res.send('review_id is null');
    }
    else {
        review_dal.delete(req.query.review_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/review/all');
            }
        });
    }
});

module.exports = router;
