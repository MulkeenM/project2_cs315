var express = require('express');
var router = express.Router();
var multiTeam_dal = require('../model/multiTeam_dal');



// View All companys
router.get('/all', function(req, res) {
    multiTeam_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('multiTeam/multiTeamViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        multiTeam_dal.getById(req.query.team_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('multiTeam/multiTeamViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    multiTeam_dal.getMultiGames(function(err, result) {

        if (err) {
            res.send(err);
        }
        else {
            res.render('multiTeam/multiTeamAdd', {'game': result});
        }

    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.team_name == null) {
        res.send('Team name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        multiTeam_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/multiTeam/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('A account id is required');
    }
    else {
        game_dal.edit(req.query.account_id, function(err, result){
            res.render('game/gameUpdate', {game: result[0][0]});
        });
    }

});


router.get('/update', function(req, res) {
    game_dal.update(req.query, function(err, result){
        res.redirect(302, '/game/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        multiTeam_dal.delete(req.query.team_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/multiTeam/all');
            }
        });
    }
});

module.exports = router;
